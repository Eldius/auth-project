package net.eldiosantos.auth.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by Eldius on 14/11/2014.
 */
public class UserProfile {
    private Long id;
    private User user;
    private Profile profile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
