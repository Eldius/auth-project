package net.eldiosantos.auth.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * User credentials.
 * Created by Eldius on 14/11/2014.
 */
public class UserCredentials {
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
