package net.eldiosantos.auth.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * User representation.
 * Created by Eldius on 14/11/2014.
 */
public class User {

    private Long id;
    private UserCredentials credentials;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserCredentials getCredentials() {
        return credentials;
    }

    public void setCredentials(UserCredentials credentials) {
        this.credentials = credentials;
    }

    public String getLogin() {
        return credentials.getLogin();
    }

    public void setLogin(String login) {
        credentials.setLogin(login);
    }

    public String getPassword() {
        return credentials.getPassword();
    }

    public void setPassword(String password) {
        credentials.setPassword(password);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
