package net.eldiosantos.auth.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by Eldius on 14/11/2014.
 */
public class Profile {

    private Long id;
    private String desplayName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesplayName() {
        return desplayName;
    }

    public void setDesplayName(String desplayName) {
        this.desplayName = desplayName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
