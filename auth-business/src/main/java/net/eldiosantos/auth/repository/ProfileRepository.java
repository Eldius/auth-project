package net.eldiosantos.auth.repository;

import net.eldiosantos.auth.model.Profile;
import net.eldiosantos.auth.repository.interfaces.Repository;

/**
 * Created by Eldius on 14/11/2014.
 */
public interface ProfileRepository extends Repository<Profile, Long> {
}
