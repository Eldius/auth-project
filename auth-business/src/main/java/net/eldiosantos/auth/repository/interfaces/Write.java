package net.eldiosantos.auth.repository.interfaces;

import java.io.Serializable;

/**
 * Interface to define Repository of elements who can be persisted.
 * @author Eldius
 *
 * @param <T>
 * @param <K>
 */
public interface Write<T, K extends Serializable>
{
    void persist(T element);

	void update(T element);
}