package net.eldiosantos.auth.repository.interfaces;

import java.io.Serializable;
import java.util.List;

/*
 * Interface to define Repository of readable elements. 
 */
public interface Read<T, K extends Serializable>
{
    T getByPk(K pk);
    List<T>listAll();
}