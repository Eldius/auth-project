package net.eldiosantos.auth.repository.interfaces;

import java.io.Serializable;

/**
 * Interface definition of repósitory for elements 
 * who can be removed from persistence.
 * @author Eldius
 *
 * @param <T>
 * @param <K>
 */
public interface Delete<T, K extends Serializable>
{
    void delete(T element);
}
