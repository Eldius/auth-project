package net.eldiosantos.auth.repository.interfaces;

import java.io.Serializable;

/**
 * Interface definition for elements who needs 
 * a complete CRUD support.
 * @author Eldius
 *
 * @param <T>
 * @param <K>
 */
public interface Repository<T, K extends Serializable> extends Read<T, K>, Write<T, K>, Delete<T, K>
{
    
}